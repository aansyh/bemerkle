CREATE TABLE guest_form (
  id serial primary key,
  nama varchar(32),
  no_telp varchar(16),
  alamat text,
  catatan text,
  is_deleted boolean default false
)