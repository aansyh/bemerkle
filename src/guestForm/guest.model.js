const { DataTypes } = require("sequelize");

const pgsql = require("../database");

const guestFormModel = pgsql.define(
	"guest_form",
	{
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true,
		},
		nama: {
			type: DataTypes.STRING,
		},
		no_telp: {
			type: DataTypes.STRING,
		},
		alamat: {
			type: DataTypes.TEXT,
		},
		catatan: {
			type: DataTypes.TEXT,
		},
	},
	{
		freezeTableName: true,
		timestamps: false,
		paranoid: false,
	}
);

module.exports = guestFormModel;
