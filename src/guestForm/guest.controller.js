const { response } = require("../helper/responseHelper");
const { CrudService } = require("../helper/crudServiceHelper");

const guestFormModel = require("./guest.model");
const { guestFormService } = require("./guest.service");

const createData = async (req, res) => {
	try {
		console.log("CREATING DATA with request: ", req.body);

		const createData = await CrudService.createData(req.body, guestFormModel);

		return response(res, 200, "OK", "Success Input", createData, null);
	} catch (error) {
		console.error(error);
		if (error.name === "ValidationError") {
			return response(res, 400, "Bad Request", null, {}, error.message);
		} else if (error.name === "NotFoundError") {
			return response(res, 404, "Not Found", null, {}, error.message);
		}
		return response(res, 500, "Internal Server Error", null, {}, error.message);
	}
};

const getAllData = async (req, res) => {
	try {
		console.log("GETTING ALL DATA with request: ", req.query);

		let { limit, page, search, order, filter } = req.query;

		!filter ? (filter = {}) : (filter = JSON.parse(filter));

		const getAllData = await CrudService.getAllData(limit, page, search, order, filter, guestFormModel);

		return response(res, 200, "OK", "Success Get All Data", getAllData, null);
	} catch (error) {
		console.error(error);
		if (error.name === "ValidationError") {
			return response(res, 400, "Bad Request", null, {}, error.message);
		} else if (error.name === "NotFoundError") {
			return response(res, 404, "Not Found", null, {}, error.message);
		}
		return response(res, 500, "Internal Server Error", null, {}, error.message);
	}
};

const getDataById = async (req, res) => {
	try {
		console.log("GETTING DATA with id ", req.params);

		const getDataById = await CrudService.getDataById(req.params.id, guestFormModel);

		return response(res, 200, "OK", "Success Get Data", getDataById, null);
	} catch (error) {
		console.error(error);
		if (error.name === "ValidationError") {
			return response(res, 400, "Bad Request", null, {}, error.message);
		} else if (error.name === "NotFoundError") {
			return response(res, 404, "Not Found", null, {}, error.message);
		}
		return response(res, 500, "Internal Server Error", null, {}, error.message);
	}
};

const updateData = async (req, res) => {
	try {
		let { id } = req.query;

		const updateData = await CrudService.updateData(id, req.body, guestFormModel)
			.then((result) => {
				return result;
			})
			.catch((err) => {
				throw err;
			});
		return response(res, 200, "OK", "Success Update Data", updateData);
	} catch (error) {
		console.error(error);
		if (error.name === "ValidationError") {
			return response(res, 400, "Bad Request", null, {}, error.message);
		} else if (error.name === "NotFoundError") {
			return response(res, 404, "Not Found", null, {}, error.message);
		}

		return response(res, 500, "Internal Server Error", null, {}, error.message);
	}
};

const deleteData = async (req, res) => {
	try {
		let { id } = req.query;

		const deleteData = await CrudService.deleteData(id, guestFormModel)
			.then((result) => {
				return result;
			})
			.catch((error) => {
				throw error;
			});

		return response(res, 200, "OK", "Success Delete Data", deleteData);
	} catch (error) {
		console.error(error);
		if (error.name === "ValidationError") {
			return response(res, 400, "Bad Request", null, {}, error.message);
		} else if (error.name === "NotFoundError") {
			return response(res, 404, "Not Found", null, {}, error.message);
		}

		return response(res, 500, "Internal Server Error", null, {}, error.message);
	}
};

module.exports = {
	createData,
	getAllData,
	getDataById,
	deleteData,
	updateData,
};
