const express = require("express");
const router = express.Router();

const guestForm = require("./guestForm/guest.controller");
const noteGallery = require("./noteGallery/note.controller");

router.post("/guest-form", guestForm.createData);
router.get("/guest-form", guestForm.getAllData);
router.get("/guest-form/:id", guestForm.getDataById);
router.put("/guest-form", guestForm.updateData);
router.delete("/guest-form", guestForm.deleteData);

router.get("/note-gallery", noteGallery.getAll);

module.exports = router;
