const { CrudService } = require("../helper/crudServiceHelper");

const guestFormModel = require("../guestForm/guest.model");

class noteService {
	static async getAll() {
		try {
			let listData = await CrudService.getAllData(null, null, null, null, null, guestFormModel);

			listData.list.forEach((x) => {
				delete x["alamat"];
				delete x["no_telp"];
			});

			return listData;
		} catch (error) {
			console.error(error);
			throw error;
		}
	}
}

module.exports = { noteService };
