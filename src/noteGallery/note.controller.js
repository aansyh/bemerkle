const { response } = require("../helper/responseHelper");
const { CrudService } = require("../helper/crudServiceHelper");

const { noteService } = require("./note.service");

const getAll = async (req, res) => {
	try {
		const getAll = await noteService.getAll();

		return response(res, 200, "OK", "Success Get", getAll, null);
	} catch (error) {
		console.error(error);
		if (error.name === "ValidationError") {
			return response(res, 400, "Bad Request", null, {}, error.message);
		} else if (error.name === "NotFoundError") {
			return response(res, 404, "Not Found", null, {}, error.message);
		}
		return response(res, 500, "Internal Server Error", null, {}, error.message);
	}
};

module.exports = {
	getAll,
};
